<#import "../parts/common.ftl" as c>

<@c.page>
    <div>
        <h3>
            Tickets
        </h3>
    </div>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Name</th>
            <th>Cost</th>
            <th>Type of seat</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <#list tickets as tc>
            <tr>
                <td>${tc.getName()}</td>
                <td>${tc.getCost()}</td>
                <td>${tc.getTypeOfSeat()}</td>
                <td><a href="/customer/repeal/${tc.getId()}">Repeal</a></td>
            </tr>
        <#else>
        </#list>
        </tbody>
    </table>
</@c.page>