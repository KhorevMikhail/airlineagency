<#macro Customer isEdit>
<#import "../parts/common.ftl" as c>

<@c.page>
    <form method="post">
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Name :</label>
            <div class="col-sm-6">
                <input type="text" name="name" class="form-control" <#if isEdit> value="${customer.getName()}"</#if> placeholder="Name" />
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Surname :</label>
            <div class="col-sm-6">
                <input type="text" name="surname" class="form-control" <#if isEdit>  value="${customer.getSurname()}" </#if> placeholder="Surname" />
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Second name :</label>
            <div class="col-sm-6">
                <input type="text" name="secondname" class="form-control" <#if isEdit>   value="${customer.getSecondname()}" </#if> placeholder="Second Name" />
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Date of birth :</label>
            <div class="col-sm-6">
                <input type="date" name="dateofbirth" class="form-control" <#if isEdit>  value="${customer.getDateofbirth()}" </#if> placeholder="Date of Birth" />
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Phone number :</label>
            <div class="col-sm-6">
                <input type="text" name="phonenumber" class="form-control" <#if isEdit>  value="${customer.getPhonenumber()}" </#if> placeholder="Phone number" />
            </div>
        </div>
        <input type="hidden" name="_csrf" value="${_csrf.token}" />
        <button class="btn btn-primary" type="submit"><#if isEdit>Edit<#else>Save</#if></button>
        <#if message??>
            ${message}
        </#if>
    </form>
</@c.page>
</#macro>
