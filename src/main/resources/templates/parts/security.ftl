<#assign
known = Session.SPRING_SECURITY_CONTEXT??
>

<#if known>
    <#assign
    account = Session.SPRING_SECURITY_CONTEXT.authentication.principal
    name = account.getUsername()
    isAdmin = account.isAdmin()
    >
<#else>
    <#assign
    name = "unknown"
    isAdmin = false
    >
</#if>