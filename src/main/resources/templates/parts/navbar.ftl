<#include "security.ftl">
<#import "login.ftl" as l>

<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <a class="navbar-brand" href="/home">AirlineAgency</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="/home">Home</a>
            </li>
            <#if isAdmin>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Add
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a href="/add/airports">Airport</a>
                        <a href="/add/airplanes">Airplane</a>
                        <a href="/add/tickets">Ticket</a>
                        <a href="/add/places">Place</a>
                        <a href="/add/flights">Flight</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Search
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a href="/search/airport">Airport</a>
                        <a href="/search/airplane">Airplane</a>
                        <a href="/search/ticket">Ticket</a>
                        <a href="/search/place">Place</a>
                        <a href="/search/flight">Flight</a>
                    </div>
                </li>
            </#if>
            <#if !isAdmin>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Customer
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a href="/customer/add">Personal information</a>
                        <a href="/customer/edit">Edit</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/customer/flight">Flight</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/customer/tickets">Ticket</a>
                </li>
            </#if>
        </ul>
        <div class="navbar-text mr-3">${name}</div>
        <@l.logout />
    </div>
</nav>