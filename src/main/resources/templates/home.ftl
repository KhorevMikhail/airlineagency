<#import "parts/common.ftl" as c>

<@c.page>
    <div class="container">

        <div class="starter-template">
            <h1>
                Welcome!
            </h1>
            <p class="lead">
                Flights and airline tickets on AirlineAgency
            </p>
        </div>
    </div>
</@c.page>