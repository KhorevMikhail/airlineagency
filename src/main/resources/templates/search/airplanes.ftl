<#import "../parts/common.ftl" as c>

<@c.page>
    <div>
        <h3>
            Airplane List
        </h3>
    </div>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Name</th>
            <th>Registration number</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <#list airplanes as ap>
            <tr>
                <td>${ap.getName()}</td>
                <td>${ap.getRegNumber()}</td>
                <td><a href="/edit/airplanes/${ap.getId()}">Edit</a></td>
            </tr>
        </#list>
        </tbody>
    </table>
</@c.page>