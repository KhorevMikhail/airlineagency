<#import "../parts/common.ftl" as c>

<@c.page>
    <div>
        <h3>
            Airport List
        </h3>
    </div>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Name</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <#list airports as ap>
            <tr>
                <td>${ap.getName()}</td>
                <td><a href="/edit/airports/${ap.getId()}">Edit</a></td>
            </tr>
        </#list>
        </tbody>
    </table>
</@c.page>