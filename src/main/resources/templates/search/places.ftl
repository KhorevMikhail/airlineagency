<#import "../parts/common.ftl" as c>

<@c.page>
    <div>
        <h3>
            Place List
        </h3>
    </div>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Country</th>
            <th>Region</th>
            <th>City</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <#list places as pl>
            <tr>
                <td>${pl.getCountry()}</td>
                <td>${pl.getRegion()}</td>
                <td>${pl.getCity()}</td>
                <td><a href="/edit/places/${pl.getId()}">Edit</a></td>
            </tr>
        </#list>
        </tbody>
    </table>
</@c.page>