<#import "../parts/common.ftl" as c>

<@c.page>
    <div>
        <h3>
            Flight List
        </h3>
    </div>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Name</th>
            <th>Date of arrival</th>
            <th>Date of departure</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <#list flights as fl>
            <tr>
                <td>${fl.getName()}</td>
                <td>${fl.getDateofarrival()}</td>
                <td>${fl.getDateofdeparture()}</td>
                <td><a href="/edit/flights/${fl.getId()}">Edit</a></td>
            </tr>
        </#list>
        </tbody>
    </table>
</@c.page>