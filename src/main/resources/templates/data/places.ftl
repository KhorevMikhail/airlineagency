<#macro places isEdit>
<#import "../parts/common.ftl" as c>

<@c.page>
    <form method="post">
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Country :</label>
            <div class="col-sm-6">
                <input type="text" name="country" class="form-control" <#if isEdit> value="${geography.getCountry()}" </#if>placeholder="Country" />
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Region :</label>
            <div class="col-sm-6">
                <input type="text" name="region" class="form-control" <#if isEdit> value="${geography.getRegion()}" </#if>placeholder="Region" />
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">City :</label>
            <div class="col-sm-6">
                <input type="text" name="city" class="form-control" <#if isEdit> value="${geography.getCity()}" </#if>placeholder="City" />
            </div>
        </div>
        <input type="hidden" name="_csrf" value="${_csrf.token}" />
        <button class="btn btn-primary" type="submit"> <#if isEdit>Edit <#else> Save</#if></button>
        <#if message??>
            ${message}
        </#if>
    </form>

</@c.page>
</#macro>