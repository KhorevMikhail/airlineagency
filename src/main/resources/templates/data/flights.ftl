<#macro flights isEdit>
<#import "../parts/common.ftl" as c>

<@c.page>
    <form method="post">
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Name :</label>
            <div class="col-sm-6">
                <input type="text" name="name" class="form-control" <#if isEdit> value="${flight.getName()}" </#if>placeholder="Name" />
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Date of arrival :</label>
            <div class="col-sm-6">
                <input type="datetime-local" name="dateofarrival" class="form-control" <#if isEdit> value="${dateofarrival}" </#if>placeholder="Date of arrival" />
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Date of departure :</label>
            <div class="col-sm-6">
                <input type="datetime-local" name="dateofdeparture" class="form-control" <#if isEdit> value="${dateofdeparture}" </#if>placeholder="Date of departure" />
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Airport of arrival :</label>
            <div class="col-sm-6">
                <input id="airportofarrival" type="text" name="airportofarrival" class="form-control" <#if isEdit> value="${airportofarrival}" </#if>placeholder="Airport of arrival" />
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Airport of departure :</label>
            <div class="col-sm-6">
                <input id="airportofdeparture" type="text" name="airportofdeparture" class="form-control" <#if isEdit> value="${airportofdeparture}" </#if>placeholder="Airport of departure" />
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Airplane :</label>
            <div class="col-sm-6">
                <input id="airplane" type="text" name="airplane" class="form-control" <#if isEdit> value="${airplane}" </#if>placeholder="Airplane" />
            </div>
        </div>
        <input type="hidden" name="_csrf" value="${_csrf.token}" />
        <button class="btn btn-primary" type="submit"> <#if isEdit> Edit <#else> Save</#if></button>
        <#if message??>
            ${message}
        </#if>
        <script>
            function autocomplete(inp, arr) {
                /*the autocomplete function takes two arguments,
                the text field element and an array of possible autocompleted values:*/
                var currentFocus;
                /*execute a function when someone writes in the text field:*/
                inp.addEventListener("input", function(e) {
                    var a, b, i, val = this.value;
                    /*close any already open lists of autocompleted values*/
                    closeAllLists();
                    if (!val) { return false;}
                    currentFocus = -1;
                    /*create a DIV element that will contain the items (values):*/
                    a = document.createElement("DIV");
                    a.setAttribute("id", this.id + "autocomplete-list");
                    a.setAttribute("class", "autocomplete-items");
                    /*append the DIV element as a child of the autocomplete container:*/
                    this.parentNode.appendChild(a);
                    /*for each item in the array...*/
                    for (i = 0; i < arr.length; i++) {
                        /*check if the item starts with the same letters as the text field value:*/
                        if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                            /*create a DIV element for each matching element:*/
                            b = document.createElement("DIV");
                            /*make the matching letters bold:*/
                            b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                            b.innerHTML += arr[i].substr(val.length);
                            /*insert a input field that will hold the current array item's value:*/
                            b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                            /*execute a function when someone clicks on the item value (DIV element):*/
                            b.addEventListener("click", function(e) {
                                /*insert the value for the autocomplete text field:*/
                                inp.value = this.getElementsByTagName("input")[0].value;
                                /*close the list of autocompleted values,
                                (or any other open lists of autocompleted values:*/
                                closeAllLists();
                            });
                            a.appendChild(b);
                        }
                    }
                });
                /*execute a function presses a key on the keyboard:*/
                inp.addEventListener("keydown", function(e) {
                    var x = document.getElementById(this.id + "autocomplete-list");
                    if (x) x = x.getElementsByTagName("div");
                    if (e.keyCode == 40) {
                        /*If the arrow DOWN key is pressed,
                        increase the currentFocus variable:*/
                        currentFocus++;
                        /*and and make the current item more visible:*/
                        addActive(x);
                    } else if (e.keyCode == 38) { //up
                        /*If the arrow UP key is pressed,
                        decrease the currentFocus variable:*/
                        currentFocus--;
                        /*and and make the current item more visible:*/
                        addActive(x);
                    } else if (e.keyCode == 13) {
                        /*If the ENTER key is pressed, prevent the form from being submitted,*/
                        e.preventDefault();
                        if (currentFocus > -1) {
                            /*and simulate a click on the "active" item:*/
                            if (x) x[currentFocus].click();
                        }
                    }
                });
                function addActive(x) {
                    /*a function to classify an item as "active":*/
                    if (!x) return false;
                    /*start by removing the "active" class on all items:*/
                    removeActive(x);
                    if (currentFocus >= x.length) currentFocus = 0;
                    if (currentFocus < 0) currentFocus = (x.length - 1);
                    /*add class "autocomplete-active":*/
                    x[currentFocus].classList.add("autocomplete-active");
                }
                function removeActive(x) {
                    /*a function to remove the "active" class from all autocomplete items:*/
                    for (var i = 0; i < x.length; i++) {
                        x[i].classList.remove("autocomplete-active");
                    }
                }
                function closeAllLists(elmnt) {
                    /*close all autocomplete lists in the document,
                    except the one passed as an argument:*/
                    var x = document.getElementsByClassName("autocomplete-items");
                    for (var i = 0; i < x.length; i++) {
                        if (elmnt != x[i] && elmnt != inp) {
                            x[i].parentNode.removeChild(x[i]);
                        }
                    }
                }
                /*execute a function when someone clicks in the document:*/
                document.addEventListener("click", function (e) {
                    closeAllLists(e.target);
                });
            }
            var airportd = [<#list arrival as ar>"${ar}"<#sep>,</#list>];
            var airporta = [<#list departure as dp>"${dp}"<#sep>,</#list>];
            var airplane = [<#list airplanes as ap>"${ap}"<#sep>,</#list>];
            autocomplete(document.getElementById("airportofarrival"), airporta);
            autocomplete(document.getElementById("airportofdeparture"), airportd);
            autocomplete(document.getElementById("airplane"), airplane);
        </script>
    </form>
</@c.page>
</#macro>