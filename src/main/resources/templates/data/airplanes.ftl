<#macro airplanes isEdit>
<#import "../parts/common.ftl" as c>
    <@c.page>
        <form method="post">
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Name :</label>
                <div class="col-sm-6">
                    <input type="text" name="name" class="form-control" <#if isEdit> value="${airplane.getName()}" </#if>placeholder="Name" />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Registration number :</label>
                <div class="col-sm-6">
                    <input type="text" name="regNumber" class="form-control" <#if isEdit> value="${airplane.getRegNumber()}" </#if>placeholder="Registration number" />
                </div>
            </div>
            <input type="hidden" name="_csrf" value="${_csrf.token}" />
            <button class="btn btn-primary" type="submit"> <#if isEdit>Edit <#else>Save</#if></button>
            <#if message??>
                ${message}
            </#if>
        </form>
    </@c.page>
</#macro>