package com.KhorevMO.AirlineAgency.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "flight")
public class Flight {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "name", unique = true)
    private String name;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "date_of_arrival")
    private Date dateofarrival;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "date_of_departure")
    private Date dateofdeparture;

    @Column(name = "id_airport_arrival")
    private Long idAirportArrival;

    @Column(name = "id_airport_departure")
    private Long idAirportDeparture;

    @Column(name = "id_airplane")
    private Long idAirplane;

    @Column(name = "number_of_seats")
    private Integer numberOfSeats;

    @Column(name = "number_of_available_seats")
    private Integer numberOfavAilableSeats;


    public Flight() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateofarrival() {
        return dateofarrival;
    }

    public void setDateofarrival(Date dateofarrival) {
        this.dateofarrival = dateofarrival;
    }

    public Date getDateofdeparture() {
        return dateofdeparture;
    }

    public void setDateofdeparture(Date dateofdeparture) {
        this.dateofdeparture = dateofdeparture;
    }

    public Long getIdAirportArrival() {
        return idAirportArrival;
    }

    public void setIdAirportArrival(Long idAirportArrival) {
        this.idAirportArrival = idAirportArrival;
    }

    public Long getIdAirportDeparture() {
        return idAirportDeparture;
    }

    public void setIdAirportDeparture(Long idAirportDeparture) {
        this.idAirportDeparture = idAirportDeparture;
    }

    public Long getIdAirplane() {
        return idAirplane;
    }

    public void setIdAirplane(Long idAirplane) {
        this.idAirplane = idAirplane;
    }

    public Integer getNumberOfSeats() {
        return numberOfSeats;
    }

    public void setNumberOfSeats(Integer numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }

    public Integer getNumberOfavAilableSeats() {
        return numberOfavAilableSeats;
    }

    public void setNumberOfavAilableSeats(Integer numberOfavAilableSeats) {
        this.numberOfavAilableSeats = numberOfavAilableSeats;
    }

    @Override
    public String toString() {
        return "Flight{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", dateofarrival=" + dateofarrival +
                ", dateofdeparture=" + dateofdeparture +
                ", idAirportArrival=" + idAirportArrival +
                ", idAirportDeparture=" + idAirportDeparture +
                ", idAirplane=" + idAirplane +
                ", numberOfSeats=" + numberOfSeats +
                ", numberOfavAilableSeats=" + numberOfavAilableSeats +
                '}';
    }
}
