package com.KhorevMO.AirlineAgency.repo;

import com.KhorevMO.AirlineAgency.model.Ticket;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TicketRepository extends CrudRepository<Ticket, Long> {
    Ticket getById(Long id);
    void deleteById(Long id);
    List<Ticket> getAllByIdFlightAndIdCustomerIsNull(Long idFlight);
    Ticket findByName(String name);
    List findAll();
    List<Ticket> findByIdCustomer(Long id);
}