package com.KhorevMO.AirlineAgency.repo;

import com.KhorevMO.AirlineAgency.model.Payment;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface PaymentRepository extends CrudRepository<Payment, Long> {

}