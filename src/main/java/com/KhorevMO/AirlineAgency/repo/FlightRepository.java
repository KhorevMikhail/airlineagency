package com.KhorevMO.AirlineAgency.repo;

import com.KhorevMO.AirlineAgency.model.Flight;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface FlightRepository extends CrudRepository<Flight, Long> {
    Flight getById(Long id);
    List getAllByDateofarrivalAndIdAirportArrivalAndIdAirportDepartureAndNumberOfavAilableSeatsNot(Date date, Long idAirportArrival, Long idAirportDeparture, Integer number);
    Flight findByName(String name);
    List findAll();
}