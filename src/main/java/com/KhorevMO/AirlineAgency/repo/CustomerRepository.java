package com.KhorevMO.AirlineAgency.repo;

import com.KhorevMO.AirlineAgency.model.Customer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CustomerRepository extends CrudRepository<Customer, Long> {
    Customer getById(Long id);
}