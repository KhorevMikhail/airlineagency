package com.KhorevMO.AirlineAgency.repo;

import com.KhorevMO.AirlineAgency.model.Airport;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AirportRepository extends CrudRepository<Airport, Long> {
    List<Airport> getAllByIdGeography(Long id);
    Airport findByName(String name);
    List<Airport> findAll();
    Airport getById(Long id);

}