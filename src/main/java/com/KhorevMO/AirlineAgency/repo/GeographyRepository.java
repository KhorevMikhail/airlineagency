package com.KhorevMO.AirlineAgency.repo;

import com.KhorevMO.AirlineAgency.model.Geography;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GeographyRepository extends CrudRepository<Geography, Long> {
    Geography findByCountryAndRegionAndCity(String country, String region, String city);
    List findAll();
    Geography getById(Long id);
}