package com.KhorevMO.AirlineAgency.repo;

import com.KhorevMO.AirlineAgency.model.Account;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface AccountRepository extends CrudRepository<Account, Long> {
    List<Account> findAll();
    Account findByUsername(String username);
}
