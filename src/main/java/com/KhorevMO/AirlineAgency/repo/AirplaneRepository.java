package com.KhorevMO.AirlineAgency.repo;

import com.KhorevMO.AirlineAgency.model.Airplane;
import com.KhorevMO.AirlineAgency.model.Airport;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface AirplaneRepository extends CrudRepository<Airplane, Long> {
    void deleteById(Long id);
    Airplane findByRegNumber(String RegNumber);
    List findAll();
    Airplane getById(Long id);



}
