package com.KhorevMO.AirlineAgency.service;

import com.KhorevMO.AirlineAgency.model.Customer;
import com.KhorevMO.AirlineAgency.repo.AccountRepository;
import com.KhorevMO.AirlineAgency.repo.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerServImpl implements CustomerService {

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    AccountRepository accountRepository;

    @Override
    public void save(Customer customer, String name) {
        customer.setId(accountRepository.findByUsername(name).getId());
        customerRepository.save(customer);
    }

    @Override
    public void update(Customer customer) {
        customerRepository.save(customer);
    }

    @Override
    public void delete(Long id) {
        customerRepository.deleteById(id);
    }

    @Override
    public Customer getByid(Long id) {
        return customerRepository.getById(id);
    }
}
