package com.KhorevMO.AirlineAgency.service;

import com.KhorevMO.AirlineAgency.model.Flight;
import com.KhorevMO.AirlineAgency.model.Ticket;
import com.KhorevMO.AirlineAgency.repo.FlightRepository;
import com.KhorevMO.AirlineAgency.repo.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TicketServImpl implements TicketService {

    @Autowired
    private TicketRepository ticketRepository;
    @Autowired
    private FlightRepository flightRepository;

    @Override
    public boolean save(Ticket ticket) {
        Ticket ticketfromBD = ticketRepository.findByName(ticket.getName());
        if(ticketfromBD == null) {
            Flight flight = flightRepository.getById(ticket.getIdFlight());
            flight.setNumberOfSeats(flight.getNumberOfSeats() + 1);
            flight.setNumberOfavAilableSeats(flight.getNumberOfavAilableSeats() + 1);
            ticketRepository.save(ticket);
            flightRepository.save(flight);
            return true;
        }
        return false;

    }

    @Override
    public void update(Ticket ticket) {
        ticketRepository.save(ticket);
    }

    @Override
    public void delete(Long id) {
        Flight flight = flightRepository.getById(id);
        flight.setNumberOfSeats(flight.getNumberOfSeats() - 1);
        flight.setNumberOfavAilableSeats(flight.getNumberOfavAilableSeats() - 1);
        ticketRepository.deleteById(id);
        flightRepository.save(flight);
    }

    @Override
    public void buy(Long idTicket, Long idCustomer) {
        Ticket ticket = ticketRepository.getById(idTicket);
        Flight flight = flightRepository.getById(ticket.getIdFlight());
        ticket.setIdCustomer(idCustomer);
        flight.setNumberOfavAilableSeats(flight.getNumberOfavAilableSeats() - 1);
        ticketRepository.save(ticket);
        flightRepository.save(flight);
    }

    @Override
    public void repeal(Long idTicket) {
        Ticket ticket = ticketRepository.getById(idTicket);
        Flight flight = flightRepository.getById(ticket.getIdFlight());
        ticket.setIdCustomer(null);
        flight.setNumberOfavAilableSeats(flight.getNumberOfavAilableSeats() + 1);
        ticketRepository.save(ticket);
        flightRepository.save(flight);
    }

    @Override
    public List<Ticket> search(Long idFlight) {
        return ticketRepository.getAllByIdFlightAndIdCustomerIsNull(idFlight);
    }

    @Override
    public List getTickets() {
        return ticketRepository.findAll();
    }

    @Override
    public Ticket getByid(Long id) {
        return ticketRepository.getById(id);
    }

    @Override
    public List<Ticket> getByCustomer(Long idCustomer) {
        return ticketRepository.findByIdCustomer(idCustomer);
    }

}
