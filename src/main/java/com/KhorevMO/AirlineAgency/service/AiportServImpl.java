package com.KhorevMO.AirlineAgency.service;

import com.KhorevMO.AirlineAgency.model.Airport;
import com.KhorevMO.AirlineAgency.repo.AirportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AiportServImpl implements AirportService {

    @Autowired
    AirportRepository airportRepository;

    @Override
    public boolean save(Airport airport) {

        Airport airportfromBD = airportRepository.findByName(airport.getName());
        if(airportfromBD == null) {
            airportRepository.save(airport);
            return true;
        }
        return false;
    }

    @Override
    public void update(Airport airport) {
        airportRepository.save(airport);
    }

    @Override
    public void delete(Long id) {
        airportRepository.deleteById(id);
    }

    @Override
    public List getAirports() {
        return airportRepository.findAll();
    }

    @Override
    public Long getId(String name) {
        return airportRepository.findByName(name).getId();
    }

    @Override
    public Airport getByid(Long id) {
        return airportRepository.getById(id);
    }
}
