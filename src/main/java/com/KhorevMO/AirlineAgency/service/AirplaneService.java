package com.KhorevMO.AirlineAgency.service;

import com.KhorevMO.AirlineAgency.model.Airplane;

import java.util.List;

public interface AirplaneService {
    boolean save(Airplane airplane);

    void update(Airplane airplane);

    void delete(Long id);

    List getAirplanes();

    Long getId(String regNumber);

    Airplane getByid(Long id);

}
