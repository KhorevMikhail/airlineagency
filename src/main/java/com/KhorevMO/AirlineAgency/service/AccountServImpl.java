package com.KhorevMO.AirlineAgency.service;

import com.KhorevMO.AirlineAgency.repo.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountServImpl implements AccountService {

    @Autowired
    AccountRepository accountRepository;

    @Override
    public Long getIDbyname(String name) {
        return accountRepository.findByUsername(name).getId();
    }
}
