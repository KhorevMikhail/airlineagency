package com.KhorevMO.AirlineAgency.service;

import com.KhorevMO.AirlineAgency.model.Ticket;

import java.util.List;

public interface TicketService {
    boolean save(Ticket ticket);

    void update(Ticket ticket);

    void delete(Long id);

    void buy(Long idTicket, Long idCustomer);

    void repeal(Long idTicket);

    List<Ticket> search(Long idFlight);

    List getTickets();

    Ticket getByid(Long id);

    List<Ticket> getByCustomer(Long idCustomer);
}
