package com.KhorevMO.AirlineAgency.service;

import com.KhorevMO.AirlineAgency.model.Airport;

import java.util.List;

public interface AirportService {
    boolean save(Airport airplane);

    void update(Airport airplane);

    void delete(Long id);

    List getAirports();

    Long getId(String Name);

    Airport getByid(Long id);
}
