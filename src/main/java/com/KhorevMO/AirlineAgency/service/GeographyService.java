package com.KhorevMO.AirlineAgency.service;

import com.KhorevMO.AirlineAgency.model.Geography;

import java.util.List;

public interface GeographyService {
    boolean save(Geography geography);

    void update(Geography geography);

    void delete(Long id);

    List getGeography();

    Long getId(String city, String region, String country);

    Geography getByid(Long id);
}
