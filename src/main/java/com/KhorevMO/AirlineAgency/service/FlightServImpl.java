package com.KhorevMO.AirlineAgency.service;

import com.KhorevMO.AirlineAgency.model.Airport;
import com.KhorevMO.AirlineAgency.model.Flight;
import com.KhorevMO.AirlineAgency.repo.AirportRepository;
import com.KhorevMO.AirlineAgency.repo.FlightRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class FlightServImpl implements FlightService {

    @Autowired
    private AirportRepository airportRepository;
    @Autowired
    private FlightRepository flightRepository;

    @Override
    public boolean save(Flight flight) {
        flight.setNumberOfSeats(0);
        flight.setNumberOfavAilableSeats(0);
        Flight flightfromBD = flightRepository.findByName(flight.getName());
        if(flightfromBD == null) {
            flightRepository.save(flight);
            return true;
        }
        return false;
    }

    @Override
    public void update(Flight flight) {
        flightRepository.save(flight);
    }

    @Override
    public void delete(Long id) {
        flightRepository.deleteById(id);
    }

    @Override
    public List getFlights() {
        return flightRepository.findAll();
    }

    @Override
    public Long getId(String name) {
        return flightRepository.findByName(name).getId();
    }

    @Override
    public List<Flight> search(Date date, Long idArrival, Long idDeparture) {
        List<Flight> result = new ArrayList<>();
        List<Airport> idAirportArrival = airportRepository.getAllByIdGeography(idArrival);
        List<Airport> idAirportDeparture = airportRepository.getAllByIdGeography(idDeparture);
        for (Airport arrival : idAirportArrival) {
            for (Airport departure  : idAirportDeparture) {
                result.addAll(flightRepository.getAllByDateofarrivalAndIdAirportArrivalAndIdAirportDepartureAndNumberOfavAilableSeatsNot(
                        date,
                        arrival.getId(),
                        departure.getId(),
                        0));
            }
        }
        return result;
    }

    @Override
    public Flight getByid(Long id) {
        return flightRepository.getById(id);
    }
}
