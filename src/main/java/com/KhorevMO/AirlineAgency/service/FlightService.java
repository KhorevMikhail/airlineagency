package com.KhorevMO.AirlineAgency.service;

import com.KhorevMO.AirlineAgency.model.Flight;
import com.KhorevMO.AirlineAgency.model.Ticket;

import java.util.Date;
import java.util.List;

public interface FlightService {
    boolean save(Flight flight);

    void update(Flight flight);

    void delete(Long id);

    List getFlights();

    Long getId(String name);

    List<Flight> search(Date date, Long idArrival, Long idDeparture);

    Flight getByid(Long id);

}
