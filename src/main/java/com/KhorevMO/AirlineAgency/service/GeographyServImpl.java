package com.KhorevMO.AirlineAgency.service;

import com.KhorevMO.AirlineAgency.model.Geography;
import com.KhorevMO.AirlineAgency.repo.GeographyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GeographyServImpl implements GeographyService {
    @Autowired
    GeographyRepository geographyRepository;

    @Override
    public boolean save(Geography geography) {
        Geography geographyFromDB = geographyRepository.findByCountryAndRegionAndCity(geography.getCountry(), geography.getRegion(), geography.getCity());
        if (geographyFromDB == null) {
            geographyRepository.save(geography);
            return true;
        }
        else
            return false;
    }

    @Override
    public void update(Geography geography) {
        geographyRepository.save(geography);
    }

    @Override
    public void delete(Long id) {
        geographyRepository.deleteById(id);
    }

    @Override
    public List getGeography() {
        List list = geographyRepository.findAll();
        return list;
    }

    @Override
    public Long getId(String city, String region, String country) {
        return geographyRepository.findByCountryAndRegionAndCity(country,region, city).getId();
    }

    @Override
    public Geography getByid(Long id) {
        return geographyRepository.getById(id);
    }
}
