package com.KhorevMO.AirlineAgency.service;

import com.KhorevMO.AirlineAgency.model.Customer;

public interface CustomerService {
    void save(Customer customer, String name);

    void update(Customer customer);

    void delete(Long id);

    Customer getByid(Long id);

}
