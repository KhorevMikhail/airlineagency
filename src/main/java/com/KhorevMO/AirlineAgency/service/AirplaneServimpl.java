package com.KhorevMO.AirlineAgency.service;

import com.KhorevMO.AirlineAgency.model.Airplane;
import com.KhorevMO.AirlineAgency.model.Airport;
import com.KhorevMO.AirlineAgency.repo.AirplaneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AirplaneServimpl implements AirplaneService {

    @Autowired
    AirplaneRepository airplaneRepository;

    @Override
    public boolean save(Airplane airplane) {
        Airplane airplanefromBD = airplaneRepository.findByRegNumber(airplane.getRegNumber());
        if(airplanefromBD == null) {
            airplaneRepository.save(airplane);
            return true;
        }
        return false;
    }

    @Override
    public void update(Airplane airplane) {
        airplaneRepository.save(airplane);
    }

    @Override
    public void delete(Long id) {
        airplaneRepository.deleteById(id);
    }

    @Override
    public List getAirplanes() {
        return airplaneRepository.findAll();
    }

    @Override
    public Long getId(String regNumber) {
        return airplaneRepository.findByRegNumber(regNumber).getId();
    }

    @Override
    public Airplane getByid(Long id) {
        return airplaneRepository.getById(id);
    }
}
