package com.KhorevMO.AirlineAgency.controllers;

import com.KhorevMO.AirlineAgency.model.*;
import com.KhorevMO.AirlineAgency.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
@PreAuthorize("hasAuthority('ADMIN')")
public class AddController {

    private String delimeter = ", ";

    @Autowired
    private GeographyService geographyService;

    @GetMapping("/add/places")
    public String addGeography(){
        return "/add/places";
    }

    @PostMapping("/add/places")
    public String addGeography(Geography geography, Map<String, Object> model)
    {
        if(!geographyService.save(geography)) {
            model.put("message", "Place exists!");
        }
        return "redirect:/add/places";
    }
    @Autowired
    private AirplaneService airplaneService;

    @GetMapping("/add/airplanes")
    public String addAirplane(){
        return "/add/airplanes";
    }

    @PostMapping("/add/airplanes")
    public String addAirplane(Airplane airplane, Map<String, Object> model)
    {
        if(!airplaneService.save(airplane)) {
            model.put("message", "Airplane exists!");
        }
        return "redirect:/add/airplanes";
    }
    @Autowired
    private AirportService airportService;

    @GetMapping("/add/airports")
    public String addAirport(Map<String, Object> model){
        List<Geography> geographyList = geographyService.getGeography();
        List<String> places = new ArrayList<>();
        for (Geography geography : geographyList) {
            places.add(geography.getCity() + ", " + geography.getRegion() + ", " + geography.getCountry());
        }
        model.put("places", places);
        return "/add/airports";
    }

    @PostMapping("/add/airports")
    public String addAirport(@RequestParam("place") String place, Airport airport, Map<String, Object> model)
    {
        String[] list = place.split(delimeter);
        airport.setIdGeography(geographyService.getId(list[0], list[1], list[2]));
        if(!airportService.save(airport)) {
            model.put("message", "Airport exists!");
        }
        return "redirect:/add/airports";
    }
    @Autowired
    private FlightService flightService;

    @GetMapping("/add/flights")
    public String addFlight(Map<String, Object> model){
        List<Airplane> airplaneList = airplaneService.getAirplanes();
        List<Airport> airpotList = airportService.getAirports();
        List<String> airports = new ArrayList<>();
        List<String> airplanes = new ArrayList<>();
        for (Airport airport: airpotList) {
            airports.add(airport.getName());
        }
        for (Airplane airplane : airplaneList) {
            airplanes.add(airplane.getRegNumber() + ", " + airplane.getName());
        }
        model.put("arrival", airports);
        model.put("departure", airports);
        model.put("airplanes", airplanes);
        return "/add/flights";
    }

    @PostMapping("/add/flights")
    public String addFlight(@RequestParam("airportofarrival") String arrival,
                            @RequestParam("airportofdeparture") String departure,
                            @RequestParam("airplane") String airplane,
                            Flight flight, Map<String, Object> model)
    {
        String[] airportarrival = arrival.split(delimeter);
        String[] airportdeparture = departure.split(delimeter);
        String[] airplanereg = airplane.split(delimeter);
        flight.setIdAirportArrival(airportService.getId(airportarrival[0]));
        flight.setIdAirportDeparture(airportService.getId(airportdeparture[0]));
        flight.setIdAirplane(airplaneService.getId(airplanereg[0]));
        if(!flightService.save(flight)) {
            model.put("message", "Flight exists!");
        }
        return "redirect:/add/flights";
    }
    @Autowired
    TicketService ticketService;

    @GetMapping("/add/tickets")
    public String addTicket(Map<String, Object> model){
        List<Flight> flightList = flightService.getFlights();
        List<String> flights = new ArrayList<>();
        for (Flight flight : flightList) {
            flights.add(flight.getName());
        }
        model.put("flights", flights);
        return "/add/tickets";
    }

    @PostMapping("/add/tickets")
    public String addTicket(@RequestParam("flight") String flight, Ticket ticket, Map<String, Object> model)
    {
        String[] list = flight.split(delimeter);
        ticket.setIdFlight(flightService.getId(list[0]));
        if(!ticketService.save(ticket)) {
            model.put("message", "Ticket exists!");
        }
        return "redirect:/add/tickets";
    }

}