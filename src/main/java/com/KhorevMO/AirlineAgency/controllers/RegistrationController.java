package com.KhorevMO.AirlineAgency.controllers;

import com.KhorevMO.AirlineAgency.model.Account;
import com.KhorevMO.AirlineAgency.model.Role;
import com.KhorevMO.AirlineAgency.repo.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Collections;
import java.util.Map;

@Controller
public class RegistrationController {

    @Autowired
    private AccountRepository accountRepository;

    @GetMapping("/registration")
    public String registration(){
        return "registration";
    }

    @PostMapping("/registration")
    public String addAccount(Account account, Map<String, Object> model)
    {
        account.setRoles(Collections.singleton(Role.USER));
        Account accountFromDB = accountRepository.findByUsername(account.getUsername());
        if(accountFromDB != null){
            model.put("message", "Account exists!");
            return "registration";
        }
        else
        {
            System.out.println(account);
            accountRepository.save(account);
            return "redirect:/login";
        }
    }
}
