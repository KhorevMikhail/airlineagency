package com.KhorevMO.AirlineAgency.controllers;

import com.KhorevMO.AirlineAgency.model.Customer;
import com.KhorevMO.AirlineAgency.model.Flight;
import com.KhorevMO.AirlineAgency.model.Geography;
import com.KhorevMO.AirlineAgency.model.Ticket;
import com.KhorevMO.AirlineAgency.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@PreAuthorize("hasAuthority('USER')")
public class CustomerController {

    private String delimeter = ", ";

    @Autowired
    CustomerService customerService;

    @Autowired
    AccountService accountService;

    public String getUserName(){
        Object obj = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String name = "";
        if (obj instanceof UserDetails)
        {
            name = ((UserDetails) obj).getUsername();
        }
        else {
            name = obj.toString();
        }
        return name;
    }

    @GetMapping("/customer/add")
    public String addCustomer(){
        return "/customer/add";
    }

    @PostMapping("/customer/add")
    public String addCustomer(Customer customer, Model model)
    {
        customerService.save(customer, getUserName());
        return "redirect:/customer/add";
    }

    @GetMapping("/customer/edit")
    public String editCustomer(Model model){
        Customer customer = customerService.getByid(accountService.getIDbyname(getUserName()));
        model.addAttribute("customer", customer);
        return "/customer/edit";
    }

    @PostMapping("/customer/edit")
    public String editCustomer(Customer customer)
    {
        customerService.update(customer);
        return "redirect:/customer/edit";
    }

    @Autowired
    GeographyService geographyService;

    @GetMapping("/customer/flight")
    public String searchFlight(Model model){
        List<Geography> geographyList = geographyService.getGeography();
        List<String> place = new ArrayList<>();
        for (Geography geography : geographyList) {
            place.add(geography.getCity() + ", " + geography.getRegion() + ", " + geography.getCountry());
        }
        List<Flight> flights = new ArrayList<Flight>();
        model.addAttribute("place", place);
        model.addAttribute("flights", flights);
        return "/customer/flight";
    }

    @Autowired
    FlightService flightService;
    @Autowired
    AirportService airportService;

    @PostMapping("/customer/flight")
    public String searchFlight(@RequestParam(value = "from") String from,
                               @RequestParam(value = "to") String to,
                               @RequestParam(value = "date") @DateTimeFormat(pattern = "yyyy-MM-dd") Date date,
                               Model model)
    {
        String[] listFrom = from.split(delimeter);
        String[] listTo = to.split(delimeter);
        List<Flight> flights = flightService.search(date,
                geographyService.getId(listFrom[0], listFrom[1], listFrom[2]),
                        geographyService.getId(listTo[0], listTo[1], listTo[2]));
        model.addAttribute("flights", flights);
        List<Geography> geographyList = geographyService.getGeography();
        List<String> place = new ArrayList<>();
        for (Geography geography : geographyList) {
            place.add(geography.getCity() + ", " + geography.getRegion() + ", " + geography.getCountry());
        }
        model.addAttribute("place", place);
        return "/customer/flight";
    }

    @Autowired
    TicketService ticketService;

    @GetMapping("/customer/ticket/{id}")
    public String searchTicket(Model model,
                               @PathVariable(value = "id") Long id){
        List<Ticket> tickets = ticketService.search(id);
        model.addAttribute("tickets", tickets);
        return "/customer/ticket";
    }
    @GetMapping("/customer/buy/{id}")
    public String buyTicket(@PathVariable(value = "id") Long id, Model model){
        ticketService.buy(id, accountService.getIDbyname(getUserName()));
        return "redirect:/customer/tickets";
    }
    @GetMapping("/customer/repeal/{id}")
    public String repealTicket(@PathVariable(value = "id") Long id){
        ticketService.repeal(id);
        return "redirect:/customer/tickets";
    }
    @GetMapping("/customer/tickets")
    public String viewTicket(Model model){
        model.addAttribute("tickets", ticketService.getByCustomer(accountService.getIDbyname(getUserName())));
        return "/customer/tickets";
    }

}
