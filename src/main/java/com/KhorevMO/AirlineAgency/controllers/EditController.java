package com.KhorevMO.AirlineAgency.controllers;

import com.KhorevMO.AirlineAgency.model.*;
import com.KhorevMO.AirlineAgency.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

@Controller
@PreAuthorize("hasAuthority('ADMIN')")
public class EditController {

    private String delimeter = ", ";

    @Autowired
    private GeographyService geographyService;

    @GetMapping("/edit/places/{id}")
    public String editGeography(Model model, @PathVariable(value = "id") Long id){
        model.addAttribute("geography", geographyService.getByid(id));
        return "/edit/places";
    }

    @PostMapping("/edit/places/{id}")
    public String editGeography(@PathVariable(value = "id") Long id, Geography geography)
    {
        geography.setId(id);
        geographyService.update(geography);
        return "redirect:/search/place";
    }
    @Autowired
    private AirplaneService airplaneService;

    @GetMapping("/edit/airplanes/{id}")
    public String editAirplane(@PathVariable(value = "id") Long id, Model model){
        model.addAttribute("airplane", airplaneService.getByid(id));
        return "/edit/airplanes";
    }

    @PostMapping("/edit/airplanes/{id}")
    public String editAirplane(Airplane airplane,Model model,
                              @PathVariable(value = "id") Long id)
    {
        airplane.setId(id);
        airplaneService.update(airplane);
        return "redirect:/search/airplane";
    }

    @Autowired
    private AirportService airportService;

    @GetMapping("/edit/airports/{id}")
    public String editAirport(@PathVariable(value = "id") Long id, Model model){
        List<Geography> geographyList = geographyService.getGeography();
        List<String> places = new ArrayList<>();
        for (Geography geography : geographyList) {
            places.add(geography.getCity() + ", " + geography.getRegion() + ", " + geography.getCountry());
        }
        Geography geography = geographyService.getByid(airportService.getByid(id).getIdGeography());
        String place = geography.getCity() + ", " +
                geography.getRegion() + ", " +
                geography.getCountry();
        model.addAttribute("place", place);
        model.addAttribute("places", places);
        model.addAttribute("airport", airportService.getByid(id));
        return "/edit/airports";
    }

    @PostMapping("/edit/airports/{id}")
    public String editAirport(@PathVariable(value = "id") Long id,
                              @RequestParam("place") String place,
                              Airport airport)
    {
        String[] places = place.split(delimeter);
        airport.setId(id);
        airport.setIdGeography(geographyService.getId(places[0], places[1], places[2]));
        airportService.update(airport);
        return "redirect:/search/airport";
    }
    @Autowired
    private FlightService flightService;

    @GetMapping("/edit/flights/{id}")
    public String editFlight(Model model, @PathVariable(value = "id") Long id){
        List<Airplane> airplaneList = airplaneService.getAirplanes();
        List<Airport> airpotList = airportService.getAirports();
        List<String> airports = new ArrayList<>();
        List<String> airplanes = new ArrayList<>();
        for (Airport airport: airpotList) {
            airports.add(airport.getName());
        }
        for (Airplane airplane : airplaneList) {
            airplanes.add(airplane.getRegNumber() + ", " + airplane.getName());
        }
        String dateofarrival = flightService.getByid(id).getDateofarrival().toString();
        String dateofdeparture = flightService.getByid(id).getDateofdeparture().toString();
        String airplane = airplaneService.getByid(flightService.getByid(id).getIdAirplane()).getRegNumber()
                +  ", " + airplaneService.getByid(flightService.getByid(id).getIdAirplane()).getName();
        model.addAttribute("airportofarrival", airportService.getByid(flightService.getByid(id).getIdAirportArrival()).getName());
        model.addAttribute("airportofdeparture", airportService.getByid(flightService.getByid(id).getIdAirportDeparture()).getName());
        model.addAttribute("airplane", airplane);
        model.addAttribute("arrival", airports);
        model.addAttribute("departure", airports);
        model.addAttribute("airplanes", airplanes);
        model.addAttribute("dateofarrival", dateofarrival);
        model.addAttribute("dateofdeparture", dateofdeparture);
        model.addAttribute("flight", flightService.getByid(id));
        return "/edit/flights";
    }

    @PostMapping("/edit/flights/{id}")
    public String editFlight(@RequestParam("airportarrival") String arrival,
                            @RequestParam("airportdeparture") String departure,
                            @RequestParam("airplane") String airplane,
                            @PathVariable(value = "id") Long id,
                            Flight flight)
    {
        String[] airportarrival = arrival.split(delimeter);
        String[] airportdeparture = departure.split(delimeter);
        String[] airplanereg = airplane.split(delimeter);
        flight.setIdAirportArrival(airportService.getId(airportarrival[1]));
        flight.setIdAirportDeparture(airportService.getId(airportdeparture[1]));
        flight.setIdAirplane(airplaneService.getId(airplanereg[1]));
        flight.setId(id);
        flightService.update(flight);
        return "redirect:/search/flight";
    }
    @Autowired
    TicketService ticketService;

    @GetMapping("/edit/tickets/{id}")
    public String eitTicket(Model model, @PathVariable(value = "id") Long id){
        List<Flight> flightList = flightService.getFlights();
        List<String> flights = new ArrayList<>();
        for (Flight flight : flightList) {
            flights.add(flight.getName());
        }
        String flightName = flightService.getByid(ticketService.getByid(id).getIdFlight()).getName();
        model.addAttribute("flight", flightName);
        model.addAttribute("flights", flights);
        model.addAttribute("ticket", ticketService.getByid(id));
        return "/edit/tickets";
    }

    @PostMapping("/edit/tickets/{id}")
    public String addTicket(@RequestParam("flight") String flight,
                            Ticket ticket, @PathVariable(value = "id") Long id)
    {
        String[] list = flight.split(delimeter);
        ticket.setIdFlight(flightService.getId(list[0]));
        ticket.setId(id);
        ticketService.update(ticket);
        return "redirect:/search/ticket";
    }

}