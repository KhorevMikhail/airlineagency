package com.KhorevMO.AirlineAgency.controllers;

import com.KhorevMO.AirlineAgency.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import sun.security.krb5.internal.Ticket;

import java.util.List;

@Controller
@PreAuthorize("hasAuthority('ADMIN')")

public class SearchController {

    @Autowired
    AirportService airportService;

    @GetMapping("search/airport")
    public String SearchAirport(Model model){

        model.addAttribute("airports", airportService.getAirports());
        return "/search/airports";
    }

    @Autowired
    AirplaneService airplaneService;

    @GetMapping("search/airplane")
    public String SearchAirplane(Model model){

        model.addAttribute("airplanes", airplaneService.getAirplanes());
        return "/search/airplanes";
    }

    @Autowired
    FlightService flightService;

    @GetMapping("search/flight")
    public String SearchFlight(Model model){

        model.addAttribute("flights", flightService.getFlights());
        return "/search/flights";
    }

    @Autowired
    GeographyService geographyService;

    @GetMapping("search/place")
    public String SearchPlace(Model model){

        model.addAttribute("places", geographyService.getGeography());
        return "/search/places";
    }

    @Autowired
    TicketService ticketService;

    @GetMapping("search/ticket")
    public String SearchTicket(Model model){
        model.addAttribute("tickets", ticketService.getTickets());
        return "/search/tickets";
    }



}
