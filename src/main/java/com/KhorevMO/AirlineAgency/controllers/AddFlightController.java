package com.KhorevMO.AirlineAgency.controllers;

import com.KhorevMO.AirlineAgency.model.Airplane;
import com.KhorevMO.AirlineAgency.model.Airport;
import com.KhorevMO.AirlineAgency.model.Flight;
import com.KhorevMO.AirlineAgency.repo.AirplaneRepository;
import com.KhorevMO.AirlineAgency.repo.AirportRepository;
import com.KhorevMO.AirlineAgency.repo.FlightRepository;
import com.KhorevMO.AirlineAgency.service.AirplaneService;
import com.KhorevMO.AirlineAgency.service.AirportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Map;

@Controller
public class AddFlightController {
    @Autowired
    private FlightRepository flightRepository;

    @Autowired
    private AirportRepository airportRepository;

    @Autowired
    private AirplaneRepository airplaneRepository;

    private AirportService airportService;

    private AirplaneService airplaneService;

    @GetMapping("/addFlight")
    public String addFlight(){
        return "addFlight";
    }

    @PostMapping("/addFlight")
    public String addFlight(Flight flight, Airport airportOfArrival, Airport airportOfDeparture, Airplane airplane , Map<String, Object> model)
    {
        Airport airportofArrivalFromDB = airportRepository.findByName(airportOfArrival.getName());
        Airport airportofDepartureFromDB = airportRepository.findByName(airportOfDeparture.getName());
        Airplane airplaneFromDB = airplaneRepository.findByRegNumber(airplane.getRegNumber());

        if(airportofArrivalFromDB == null){
            airportService.save(airportOfArrival);
            airportofArrivalFromDB = airportRepository.findByName(airportOfArrival.getName());
        }
        if(airportofDepartureFromDB == null){
            airportService.save(airportOfDeparture);
            airportofDepartureFromDB = airportRepository.findByName(airportOfDeparture.getName());
        }
        if(airplaneFromDB == null){
            airplaneService.save(airplane);
            airplaneFromDB = airplaneRepository.findByRegNumber(airplane.getRegNumber());
        }

        flight.setIdAirportArrival(airportofArrivalFromDB.getId());
        flight.setIdAirportDeparture(airportofDepartureFromDB.getId());
        flight.setIdAirplane(airplaneFromDB.getId());
        flightRepository.save(flight);
        return "redirect:/addFlight";
        }
    }

