package com.KhorevMO.AirlineAgency.controllers;

import com.KhorevMO.AirlineAgency.model.Flight;
import com.KhorevMO.AirlineAgency.repo.FlightRepository;
import com.KhorevMO.AirlineAgency.service.FlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Date;
import java.util.List;
import java.util.Map;


@Controller
public class ApplicationController {
   @Autowired
   private FlightRepository flightRepository;

   private FlightService flightService;

    @GetMapping("/search")
    public String search(){
        return "search";
    }

    @GetMapping("/home")
    public String hello(Map<String, Object> model) {
        return "home";
    }

    @PostMapping("/search")
    public String search(@RequestParam Long from, @RequestParam Long to, @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd")  Date date, Map<String, Object> model){
      List<Flight> flights = flightService.search(date, from, to);
      model.put("flights", flights);
      return "search";
    }
}
