package com.KhorevMO.AirlineAgency;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AirlineAgencyApplication {

	public static void main(String[] args) {
		SpringApplication.run(AirlineAgencyApplication.class, args);
	}

}

