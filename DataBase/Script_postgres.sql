CREATE TABLE "geography" (
	"id" serial NOT NULL,
	"country" VARCHAR(255) NOT NULL,
	"region" VARCHAR(255) NOT NULL,
	"city" VARCHAR(255) NOT NULL,
	CONSTRAINT geography_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "airport" (
	"id" serial NOT NULL,
	"username" VARCHAR(255) NOT NULL,
	"address" VARCHAR(255) NOT NULL,
	"id_geography" integer NOT NULL,
	CONSTRAINT airport_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "flight" (
	"id" serial NOT NULL,
	"username" VARCHAR(255) NOT NULL,
	"dateofarrival" DATETIME NOT NULL,
	"dateofdeparture" DATETIME NOT NULL,
	"id_airport_arrival" integer NOT NULL,
	"id_airport_departure" integer NOT NULL,
	"id_airplane" integer NOT NULL,
	CONSTRAINT flight_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "airplane" (
	"id" serial NOT NULL,
	"username" VARCHAR(255) NOT NULL,
	"registration_number" VARCHAR(255) NOT NULL UNIQUE,
	CONSTRAINT airplane_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "ticket" (
	"id" serial NOT NULL,
	"username" VARCHAR(255) NOT NULL UNIQUE,
	"cost" FLOAT NOT NULL,
	"id_customer" integer NOT NULL,
	"typeofseat" VARCHAR(255) NOT NULL,
	"id_flight" integer NOT NULL,
	CONSTRAINT ticket_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "customer" (
	"id" integer NOT NULL,
	"username" VARCHAR(255) NOT NULL,
	"surname" VARCHAR(255) NOT NULL,
	"secondname" VARCHAR(255) NOT NULL,
	"date_of_birth" DATE NOT NULL,
	"phone_number" integer NOT NULL UNIQUE,
	CONSTRAINT customer_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "payment" (
	"id" serial NOT NULL,
	"id_customer" integer NOT NULL,
	"id_ticket" integer NOT NULL,
	"type" VARCHAR(255) NOT NULL,
	CONSTRAINT payment_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "account" (
	"id" serial NOT NULL,
	"username" VARCHAR(255) NOT NULL UNIQUE,
	"password" VARCHAR(255) NOT NULL,
	"role" VARCHAR(255) NOT NULL,
	CONSTRAINT account_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);




ALTER TABLE "airport" ADD CONSTRAINT "airport_fk0" FOREIGN KEY ("id_geography") REFERENCES "geography"("id");

ALTER TABLE "flight" ADD CONSTRAINT "flight_fk0" FOREIGN KEY ("id_airport_arrival") REFERENCES "airport"("id");
ALTER TABLE "flight" ADD CONSTRAINT "flight_fk1" FOREIGN KEY ("id_airport_departure") REFERENCES "airport"("id");
ALTER TABLE "flight" ADD CONSTRAINT "flight_fk2" FOREIGN KEY ("id_airplane") REFERENCES "airplane"("id");


ALTER TABLE "ticket" ADD CONSTRAINT "ticket_fk0" FOREIGN KEY ("id_customer") REFERENCES "customer"("id");
ALTER TABLE "ticket" ADD CONSTRAINT "ticket_fk1" FOREIGN KEY ("id_flight") REFERENCES "flight"("id");

ALTER TABLE "customer" ADD CONSTRAINT "customer_fk0" FOREIGN KEY ("id") REFERENCES "account"("id");

ALTER TABLE "payment" ADD CONSTRAINT "payment_fk0" FOREIGN KEY ("id_customer") REFERENCES "customer"("id");
ALTER TABLE "payment" ADD CONSTRAINT "payment_fk1" FOREIGN KEY ("id_ticket") REFERENCES "ticket"("id");


