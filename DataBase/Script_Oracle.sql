CREATE TABLE "customer" (
	"id" INT,
	"username" VARCHAR2,
	"surname" VARCHAR2,
	"secondname" VARCHAR2,
	"dateofbirth" DATE,
  "passport" VARCHAR2,
	"phonenumber" VARCHAR2,
	constraint CUSTOMER_PK PRIMARY KEY ("id")
CREATE sequence "CUSTOMER_SEQ"
/
CREATE trigger "BI_CUSTOMER"
  before insert on "CUSTOMER"
  for each row
begin
  select "CUSTOMER_SEQ".nextval into :NEW."id" from dual;
end;
/

)
/
CREATE TABLE "account" (
	"id" INT,
	"username" VARCHAR2,
	"password" VARCHAR2,
	constraint ACCOUNT_PK PRIMARY KEY ("id")
CREATE sequence "ACCOUNT_SEQ"
/
CREATE trigger "BI_ACCOUNT"
  before insert on "ACCOUNT"
  for each row
begin
  select "ACCOUNT_SEQ".nextval into :NEW."id" from dual;
end;
/

)
/
CREATE TABLE "ticket" (
	"id" INT,
	"username" VARCHAR2,
	"cost" FLOAT,
	"id_customer" INT,
	"typeofseat" VARCHAR2,
	"id_flight" INT,
	constraint TICKET_PK PRIMARY KEY ("id")
CREATE sequence "TICKET_SEQ"
/
CREATE trigger "BI_TICKET"
  before insert on "TICKET"
  for each row
begin
  select "TICKET_SEQ".nextval into :NEW."id" from dual;
end;
/

)
/
CREATE TABLE "payment" (
	"id" INT,
	"id_customer" INT,
	"id_ticket" INT,
	"type" VARCHAR2,
	constraint PAYMENT_PK PRIMARY KEY ("id")
CREATE sequence "PAYMENT_SEQ"
/
CREATE trigger "BI_PAYMENT"
  before insert on "PAYMENT"
  for each row
begin
  select "PAYMENT_SEQ".nextval into :NEW."id" from dual;
end;
/

)
/
CREATE TABLE "flight" (
	"id" INT,
	"username" VARCHAR2,
	"dateofarrival" DATETIME,
	"dateofdeparture" DATETIME,
	"id_airport_arrival" INT,
	"id_airport_departure" INT,
	"id_airplane" INT,
	constraint FLIGHT_PK PRIMARY KEY ("id")
CREATE sequence "FLIGHT_SEQ"
/
CREATE trigger "BI_FLIGHT"
  before insert on "FLIGHT"
  for each row
begin
  select "FLIGHT_SEQ".nextval into :NEW."id" from dual;
end;
/

)
/
CREATE TABLE "airport" (
	"id" INT,
	"username" INT,
	"address" VARCHAR2,
	"id_geography" VARCHAR2,
	constraint AIRPORT_PK PRIMARY KEY ("id")
CREATE sequence "AIRPORT_SEQ"
/
CREATE trigger "BI_AIRPORT"
  before insert on "AIRPORT"
  for each row
begin
  select "AIRPORT_SEQ".nextval into :NEW."id" from dual;
end;
/

)
/
CREATE TABLE "geography" (
	"id" INT,
	"country" VARCHAR2,
	"region" VARCHAR2,
	"city" VARCHAR2,
	constraint GEOGRAPHY_PK PRIMARY KEY ("id")
CREATE sequence "GEOGRAPHY_SEQ"
/
CREATE trigger "BI_GEOGRAPHY"
  before insert on "GEOGRAPHY"
  for each row
begin
  select "GEOGRAPHY_SEQ".nextval into :NEW."id" from dual;
end;
/

)
/
CREATE TABLE "airplane" (
	"id" INT,
	"username" VARCHAR2,
	"registration_number" VARCHAR2,
	constraint AIRPLANE_PK PRIMARY KEY ("id")
CREATE sequence "AIRPLANE_SEQ"
/
CREATE trigger "BI_AIRPLANE"
  before insert on "AIRPLANE"
  for each row
begin
  select "AIRPLANE_SEQ".nextval into :NEW."id" from dual;
end;
/

)
/
CREATE TABLE "manager" (
	"id" INT,
	"username" VARCHAR2,
	constraint MANAGER_PK PRIMARY KEY ("id")
CREATE sequence "MANAGER_SEQ"
/
CREATE trigger "BI_MANAGER"
  before insert on "MANAGER"
  for each row
begin
  select "MANAGER_SEQ".nextval into :NEW."id" from dual;
end;
/

)
/
ALTER TABLE "customer" ADD CONSTRAINT "customer_fk0" FOREIGN KEY ("id") REFERENCES account("id");


ALTER TABLE "ticket" ADD CONSTRAINT "ticket_fk0" FOREIGN KEY ("id_customer") REFERENCES customer("id");
ALTER TABLE "ticket" ADD CONSTRAINT "ticket_fk1" FOREIGN KEY ("id_flight") REFERENCES flight("id");

ALTER TABLE "payment" ADD CONSTRAINT "payment_fk0" FOREIGN KEY ("id_customer") REFERENCES customer("id");
ALTER TABLE "payment" ADD CONSTRAINT "payment_fk1" FOREIGN KEY ("id_ticket") REFERENCES ticket("id");

ALTER TABLE "flight" ADD CONSTRAINT "flight_fk0" FOREIGN KEY ("id_airport_arrival") REFERENCES airport("id");
ALTER TABLE "flight" ADD CONSTRAINT "flight_fk1" FOREIGN KEY ("id_airport_departure") REFERENCES airport("id");
ALTER TABLE "flight" ADD CONSTRAINT "flight_fk2" FOREIGN KEY ("id_airplane") REFERENCES airplane("id");

ALTER TABLE "airport" ADD CONSTRAINT "airport_fk0" FOREIGN KEY ("id_geography") REFERENCES geography("id");



ALTER TABLE "manager" ADD CONSTRAINT "manager_fk0" FOREIGN KEY ("id") REFERENCES account("id");

